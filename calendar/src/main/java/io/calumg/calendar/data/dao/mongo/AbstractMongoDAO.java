package io.calumg.calendar.data.dao.mongo;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.exists;
import static com.mongodb.client.model.Filters.gt;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Filters.not;
import static com.mongodb.client.model.Filters.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import io.calumg.calendar.data.dao.Filter;
import io.calumg.calendar.data.dao.GenericDAO;
import io.calumg.calendar.data.dao.mongo.document.DocumentBuilder;

public abstract class AbstractMongoDAO<T> implements GenericDAO<T, ObjectId> {

	protected MongoClient mongoClient;
	/* Database selected by full implementations */
	protected MongoDatabase mongoDatabase;
	/* Collection selected by full implementations */
	protected MongoCollection<Document> mongoCollection;
	/*
	 * Document builder for specific return class should be set in full
	 * implementations
	 */
	protected DocumentBuilder<T> documentBuilder;
	
	private Logger logger = LogManager.getLogger(this.getClass());

	public AbstractMongoDAO(MongoClient client) {
		this.mongoClient = client;
	}

	@Override
	public List<T> searchByCriteria(Map<String, List<Filter>> criteria) {
		logger.debug("Searching by criteria: " + criteria);
		List<T> returnedDocuments = new ArrayList<T>();
		List<Bson> filters = new ArrayList<Bson>();
		for (Entry<String, List<Filter>> entry : criteria.entrySet()) {
			String field = entry.getKey();
			for (Filter filter : entry.getValue()) {
				switch (filter.getType()) {
				case EQ:
					filters.add(eq(field, filter.getValue()));
					break;
				case EXISTS:
					filters.add(exists(field));
					break;
				case GT:
					filters.add(gt(field, filter.getValue()));
					break;
				case GTE:
					filters.add(gte(field, filter.getValue()));
					break;
				case LT:
					filters.add(lt(field, filter.getValue()));
					break;
				case LTE:
					filters.add(lte(field, filter.getValue()));
					break;
				case LIKE:
					filters.add(regex(field, Pattern.compile("(" + filter.getValue() + ")")));
					break;
				case NOTEQ:
					filters.add(not(eq(field, filter.getValue())));
					break;
				default:
					break;
				}
			}
		}
		MongoCursor<Document> cursor = mongoCollection.find(and(filters.toArray(new Bson[filters.size()]))).iterator();
		while (cursor.hasNext()) {
			returnedDocuments.add(documentBuilder.fromDocument(cursor.next()));
		}
		return returnedDocuments;
	}

}
