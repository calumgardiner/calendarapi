package io.calumg.calendar.data.dao;

import java.util.List;
import java.util.Map;

public interface GenericDAO<T, K> {

	public void insert(T entity);

	public T getById(K id);
	
	public void update(T entity);

	public void remove(K id);

	public List<T> searchByCriteria(Map<String, List<Filter>> criteria);

}
