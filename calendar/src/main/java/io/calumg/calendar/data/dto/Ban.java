package io.calumg.calendar.data.dto;

public class Ban {

	private boolean isBanned;
	private String banReason;

	public Ban() {
	}

	public Ban(boolean isBanned, String banReason) {
		this.isBanned = isBanned;
		this.banReason = banReason;
	}

	public boolean isBanned() {
		return isBanned;
	}

	public void setBanned(boolean isBanned) {
		this.isBanned = isBanned;
	}

	public String getBanReason() {
		return banReason;
	}

	public void setBanReason(String banReason) {
		this.banReason = banReason;
	}

}
