package io.calumg.calendar.data.dao;

public class Filter {

	private FilterType type;
	private Object value;

	public Filter() {

	}

	public Filter(FilterType type, Object value) {
		this(type);
		this.value = value;
	}

	public Filter(FilterType type) {
		this.type = type;
	}

	public FilterType getType() {
		return type;
	}

	public void setType(FilterType type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
