package io.calumg.calendar.data.dao.mongo;

import org.bson.types.ObjectId;

import com.mongodb.MongoClient;

import io.calumg.calendar.data.dao.DAOStrategy;
import io.calumg.calendar.data.dao.UserDAO;
import io.calumg.calendar.data.dto.mongo.MongoUser;

/**
 * 
 * @author calum
 *
 */
public class MongoDAOStrategy implements DAOStrategy<ObjectId> {
	
	private MongoClient client;
	
	public MongoDAOStrategy(MongoClient client) {
		this.client = client;
	}
	
	@Override
	public UserDAO<MongoUser, ObjectId> getUserDAO() {
		return new MongoUserDAO(client);
	}

}
