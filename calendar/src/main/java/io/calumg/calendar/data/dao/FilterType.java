package io.calumg.calendar.data.dao;

public enum FilterType {
	
	EQ, EXISTS, GT, GTE, LT, LTE, LIKE, NOTEQ;

}
