package io.calumg.calendar.data.dao;

import io.calumg.calendar.data.dto.User;

public interface UserDAO<U extends User, K> extends GenericDAO<U, K> {

	public U getUserByUsername(String username);

	public U getUserByEmail(String email);
	
}
