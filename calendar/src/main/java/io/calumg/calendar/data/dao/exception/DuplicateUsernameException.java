package io.calumg.calendar.data.dao.exception;

public class DuplicateUsernameException extends Exception {

	private static final long serialVersionUID = -110480655010429016L;

	public DuplicateUsernameException(String message) {
		super(message);
	}

}
