package io.calumg.calendar.data.dao;

import io.calumg.calendar.data.dto.User;

/**
 * Interface that DAO strategies should implement, allows code to use entirely
 * different DAO strategies if required.
 * 
 * @author calum
 *
 */
public interface DAOStrategy<K> {

	public <U extends User> UserDAO<U, K> getUserDAO();

}
