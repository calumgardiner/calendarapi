package io.calumg.calendar.data.dao.mongo.document;

import org.bson.Document;

import io.calumg.calendar.data.dto.Ban;
import io.calumg.calendar.data.dto.mongo.MongoUser;

public class UserDocumentBuilder implements DocumentBuilder<MongoUser> {

	public Document buildUserDocument(MongoUser user) {
		Document document = new Document("username", user.getUsername())
				.append("email", user.getEmail())
				.append("password", user.getPassword())
				.append("deleted", user.isDeleted())
				.append("_id", user.getId())
				.append("ban", buildBanDocument(user.getBan()));
		return document;
	}

	private Document buildBanDocument(Ban ban) {
		Document document = new Document("banned", ban.isBanned())
				.append("reason", ban.getBanReason());
		return document;
	}

	@Override
	public MongoUser fromDocument(Document document) {
		if (document == null)
			return null;
		MongoUser user = new MongoUser(
				document.getObjectId("_id"), 
				document.getString("username"),
				document.getString("email"), 
				document.getString("password"), 
				document.getBoolean("deleted"));
		user.setBan(banFromDocument((Document) document.get("ban")));
		return user;
	}
	
	private Ban banFromDocument(Document document) {
		if (document == null)
			return null;
		return new Ban(
				document.getBoolean("banned"), 
				document.getString("reason")
				);
	}

}
