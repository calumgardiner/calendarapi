package io.calumg.calendar.data.dao.exception;

public class DuplicateEmailException extends Exception {

	private static final long serialVersionUID = 8594196067828481946L;

	public DuplicateEmailException(String message) {
		super(message);
	}

}
