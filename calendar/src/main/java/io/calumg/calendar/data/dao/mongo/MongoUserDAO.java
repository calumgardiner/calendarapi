package io.calumg.calendar.data.dao.mongo;

import static com.mongodb.client.model.Filters.eq;
import static io.calumg.calendar.data.dao.mongo.MongoConstants.CALENDAR_DB;
import static io.calumg.calendar.data.dao.mongo.MongoConstants.USER_COLLECTION;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;

import io.calumg.calendar.data.dao.UserDAO;
import io.calumg.calendar.data.dao.mongo.document.UserDocumentBuilder;
import io.calumg.calendar.data.dto.mongo.MongoUser;

/**
 * 
 * @author calum
 *
 */
public class MongoUserDAO extends AbstractMongoDAO<MongoUser>implements UserDAO<MongoUser, ObjectId> {

	private static UserDocumentBuilder documentBuilder = new UserDocumentBuilder();
	private Logger logger = LogManager.getLogger(this.getClass());

	public MongoUserDAO(MongoClient client) {
		super(client);
		this.mongoDatabase = this.mongoClient.getDatabase(CALENDAR_DB);
		this.mongoCollection = this.mongoDatabase.getCollection(USER_COLLECTION);
	}

	@Override
	public void insert(MongoUser user) {
		logger.debug("Inserting user: " + user);
		this.mongoCollection.insertOne(documentBuilder.buildUserDocument(user));
	}

	@Override
	public void remove(ObjectId id) {
		logger.debug("Removing user: " + id);
		MongoUser user = getById(id);
		logger.debug("Found user to remove: " + user);
		user.setDeleted(true);
		logger.debug("Setting isDeleted true");
		this.update(user);
	}

	@Override
	public MongoUser getById(ObjectId id) {
		logger.debug("Get user by id: " + id);
		Document document = this.mongoCollection.find(eq("_id", id)).first();
		MongoUser user = documentBuilder.fromDocument(document);
		logger.debug("User retrieved: " + user);
		return user;
	}

	@Override
	public MongoUser getUserByUsername(String username) {
		logger.debug("Get user by username: " + username);
		Document document = this.mongoCollection.find(eq("username", username)).first();
		MongoUser user = documentBuilder.fromDocument(document);
		logger.debug("User retrieved: " + user);
		return user;
	}

	@Override
	public MongoUser getUserByEmail(String email) {
		logger.debug("Get user by email: " + email);
		Document document = this.mongoCollection.find(eq("email", email)).first();
		MongoUser user = documentBuilder.fromDocument(document);
		logger.debug("User retrieved: " + user);
		return user;
	}

	@Override
	public void update(MongoUser user) {
		Document document = documentBuilder.buildUserDocument(user);
		this.mongoCollection.updateOne(eq("_id", user.getId()), new Document("$set", document));
	}

}
