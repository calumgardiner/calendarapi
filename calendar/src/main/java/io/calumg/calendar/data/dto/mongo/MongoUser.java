package io.calumg.calendar.data.dto.mongo;

import javax.xml.bind.annotation.XmlRootElement;

import org.bson.types.ObjectId;

import io.calumg.calendar.data.dto.User;

@XmlRootElement
public class MongoUser extends User {

	private ObjectId id;

	public MongoUser() {
	}

	public MongoUser(ObjectId id, String username, String email, String password, boolean isDeleted) {
		this.id = id;
		setUsername(username);
		setEmail(email);
		setPassword(password);
		setDeleted(isDeleted);
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "ObjectId("+this.id+"):"+super.toString();
	}

}
