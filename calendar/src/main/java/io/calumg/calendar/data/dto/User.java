package io.calumg.calendar.data.dto;

public abstract class User {

	private String username;
	private String email;
	private String password;
	private boolean isDeleted;
	private Ban ban;

	public User() {
	}

	public User(String username, String email, String password) {
		this.username = username;
		this.email = email;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return String.format("%s, %s, %s", username, email, password);
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public void setBan(Ban ban) {
		this.ban = ban;
	}
	
	public Ban getBan() {
		return this.ban;
	}
	
}
