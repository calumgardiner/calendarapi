package io.calumg.calendar.data.dao.mongo;

public class MongoConstants {
	
	public static final String CALENDAR_DB = "calendar";
	public static final String USER_COLLECTION = "users";

}
