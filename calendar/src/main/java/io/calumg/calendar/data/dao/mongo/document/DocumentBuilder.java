package io.calumg.calendar.data.dao.mongo.document;

import org.bson.Document;

public interface DocumentBuilder<T> {
	
	public T fromDocument(Document document);

}
