package io.calumg.calendar.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;

import io.calumg.calendar.data.dao.DAOStrategy;
import io.calumg.calendar.data.dao.mongo.MongoDAOStrategy;
import io.calumg.calendar.data.dto.mongo.MongoUser;
import io.calumg.calendar.ejb.UserEJB;
import io.calumg.calendar.ejb.businessif.UserIF;
import io.calumg.calendar.rest.response.Error;
import io.calumg.calendar.rest.response.ResponseUser;

@Path("/user")
public class MongoUserService {
	
	private UserIF<MongoUser, ObjectId> userEJB;
	private DAOStrategy<ObjectId> daoStrategy;
	private MongoClient mongoClient;
	private Logger logger = LogManager.getLogger(this.getClass());
	
	public MongoUserService() {
		this.mongoClient = MongoClientFactory.getMongoClient();
		this.daoStrategy = new MongoDAOStrategy(this.mongoClient);
		this.userEJB = new UserEJB<>(this.daoStrategy);
	}
	
	@GET
	@Path("/username/{param}")
	@Produces("application/json")
	public Response getUserByUsername(@PathParam("param") String username) {
		MongoUser user = null;
		try {
			user = this.userEJB.getUserByUsername(username);
		} catch (Exception e) {
			this.logger.error(e);
			return Response.status(200).entity(new Error(e.getMessage())).build();
		} finally {
			// Always close the client after returning.
			this.mongoClient.close();
		}
		return Response.status(200).entity(new ResponseUser(user)).build();
	}

}
