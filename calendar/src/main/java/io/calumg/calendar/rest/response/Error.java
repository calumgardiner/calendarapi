package io.calumg.calendar.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Error {
	
	private String error;
	
	public Error(String error) { 
		this.setError(error);
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
