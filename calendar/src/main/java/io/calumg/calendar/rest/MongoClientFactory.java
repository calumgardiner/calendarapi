package io.calumg.calendar.rest;

import com.mongodb.MongoClient;

public class MongoClientFactory {
	
	public static MongoClient getMongoClient() {
		return  new MongoClient("localhost", 27017);
	}

}
