package io.calumg.calendar.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

import io.calumg.calendar.data.dto.Ban;
import io.calumg.calendar.data.dto.mongo.MongoUser;

@XmlRootElement
public class ResponseUser {

	private String id;
	private String username;
	private String email;
	private String password;
	private boolean isDeleted;
	private Ban ban;

	public ResponseUser(MongoUser user) {
		this.id = user.getId().toHexString();
		this.username = user.getUsername();
		this.email = user.getEmail();
		this.password = user.getPassword();
		this.isDeleted = user.isDeleted();
		this.ban = user.getBan();
	}

	public ResponseUser() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Ban getBan() {
		return ban;
	}

	public void setBan(Ban ban) {
		this.ban = ban;
	}

}
