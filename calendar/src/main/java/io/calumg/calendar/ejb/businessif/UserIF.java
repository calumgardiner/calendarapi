package io.calumg.calendar.ejb.businessif;

import io.calumg.calendar.data.dto.User;

/**
 * BusinessInterface, interface describing methods available with business logic
 * around users. These methods should be usable without much work by the api
 * service / web service, which should be kept dumb as possible.
 * 
 * @author calum
 *
 * @param <U>
 */
public interface UserIF<U extends User, K> {

	public void signUp(U user) throws Exception;

	public void banUser(U user, String reason) throws Exception;

	public U getUserById(K id) throws Exception;

	public U getUserByUsername(String username);

}
