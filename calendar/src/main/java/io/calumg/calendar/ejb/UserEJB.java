package io.calumg.calendar.ejb;

import io.calumg.calendar.data.dao.DAOStrategy;
import io.calumg.calendar.data.dao.UserDAO;
import io.calumg.calendar.data.dao.exception.DuplicateEmailException;
import io.calumg.calendar.data.dao.exception.DuplicateUsernameException;
import io.calumg.calendar.data.dto.Ban;
import io.calumg.calendar.data.dto.User;
import io.calumg.calendar.ejb.businessif.UserIF;

public class UserEJB<U extends User, K> implements UserIF<U, K> {

	protected UserDAO<U, K> dao;

	public UserEJB(DAOStrategy<K> daoStrategy) {
		this.dao = daoStrategy.getUserDAO();
	}

	@Override
	public void signUp(U user) throws Exception {
		// Check unique, the client should do this actively when signing up
		// so really this should never happen
		if (this.dao.getUserByEmail(user.getEmail()) != null)
			throw new DuplicateEmailException("The email " + user.getEmail() + " is already registered to an account.");
		else if (this.dao.getUserByUsername(user.getUsername()) != null)
			throw new DuplicateUsernameException("The username " + user.getUsername() + " is already in use.");
		this.dao.insert(user);
	}

	@Override
	public void banUser(U user, String reason) throws Exception {
		user.setBan(new Ban(true, reason));
		this.dao.update(user);
	}

	@Override
	public U getUserById(K id) throws Exception {
		return this.dao.getById(id);
	}

	@Override
	public U getUserByUsername(String username) {
		return this.dao.getUserByUsername(username);
	}

}
